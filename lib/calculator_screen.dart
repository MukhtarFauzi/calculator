import 'package:flutter/material.dart';

class CalculatorScreen extends StatefulWidget {
  const CalculatorScreen({Key? key}) : super(key: key);

  @override
  _CalculatorScreenState createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  String displayText = '0';
  int actualNumber = 0;
  String? operatorSelected;
  bool needReset = false;

  int calculate() {
    int total = 0;
    if(operatorSelected == '+') {
      total = actualNumber + int.parse(displayText);
    }
    if(operatorSelected == '-') {
      total = actualNumber - int.parse(displayText);
      if(actualNumber == 0) {
        total = int.parse(displayText);
      }
    }
    if(operatorSelected == 'X') {
      total = actualNumber * int.parse(displayText);
      if(actualNumber == 0){
        total = int.parse(displayText);
    }
    }
    if(operatorSelected == '/') {
      total = actualNumber ~/ int.parse(displayText);
      if(actualNumber == 0){
        total = int.parse(displayText);
      }
    }
    print(total);
    return total;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white24,
      appBar: AppBar(
        title: Text('Calculator'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            SizedBox(height: 10,),
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 7),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
                color: Color.fromRGBO(208, 220, 207, 1),
              ),
              child: Text(
                displayText,
                textAlign: TextAlign.end,
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
            Expanded(
              flex: 3,
              child: GridView.builder(
                itemCount: 16,
                padding: EdgeInsets.symmetric(vertical: 10),
                gridDelegate:
                    SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
                itemBuilder: (BuildContext context, int index) {
                  switch(index){
                    case 0:
                      return CustomButton(
                        text: '1',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 1:
                      return CustomButton(
                        text: '2',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 2:
                      return CustomButton(
                        text: '3',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 3:
                      return CustomButton(
                        text: '+',
                        callback: (value) {
                          setState(() {
                            needReset = true;
                            operatorSelected = '+';
                            actualNumber = calculate();
                          });
                        },
                      );
                    case 4:
                      return CustomButton(
                        text: '4',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 5:
                      return CustomButton(
                        text: '5',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 6:
                      return CustomButton(
                        text: '6',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 7:
                      return CustomButton(
                        text: '-',
                        callback: (value) {
                          setState(() {
                            needReset = true;
                            operatorSelected = '-';
                            actualNumber = calculate();
                          });
                        },
                      );
                    case 8:
                      return CustomButton(
                        text: '7',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 9:
                      return CustomButton(
                        text: '8',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 10:
                      return CustomButton(
                        text: '9',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            needReset = false;
                            displayText = value;
                          });
                        },
                      );
                    case 11:
                      return CustomButton(
                        text: 'X',
                        callback: (value) {
                          setState(() {
                            needReset = true;
                            operatorSelected = 'X';
                            actualNumber = calculate();
                          });
                        },
                      );
                    case 12:
                      return CustomButton(
                        text: '0',
                        actualDisplay: displayText,
                        operatorSelected: operatorSelected,
                        needReset: needReset,
                        callback: (value) {
                          setState(() {
                            displayText = value;
                          });
                        },
                      );
                    case 13:
                      return CustomButton(
                        text: 'CE',
                        callback: (value) {
                          setState(() {
                            actualNumber = 0;
                            displayText = '0';
                          });
                        },
                      );
                    case 14:
                      return CustomButton(
                        text: '=',
                        callback: (value) {
                          setState(() {
                            displayText = calculate().toString();
                            actualNumber = 0;
                          });
                        },
                      );
                    case 15:
                      return CustomButton(
                        text: '/',
                        callback: (value) {
                          setState(() {
                            needReset = true;
                            operatorSelected = '/';
                            actualNumber = calculate();
                          });
                        },
                      );
                    default:
                      return CustomButton(
                        text: '${index + 1}',
                        callback: (value) {
                          setState(() {
                            displayText = value;
                          });
                        },
                      );
                  }


                },
              ),
            ),
            Spacer(
              flex: 2,
            )
          ],
        ),
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  final String text;
  final String actualDisplay;
  final bool needReset;
  final String? operatorSelected;
  final Function(String numboperatorSelecteder) callback;
  const CustomButton({
    Key? key, required this.text, required this.callback, this.actualDisplay = '0', this.operatorSelected, this.needReset = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: InkResponse(
        onTap: () {
          if(actualDisplay == '0' || needReset){
            callback(text);
          } else {
            callback('$actualDisplay$text');
          }
        },
        child: AspectRatio(
            aspectRatio: 4/4,
            child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10))

            ),
            child: Center(child: Text(text, style: Theme.of(context).textTheme.headline4,)))),
      ),
    );
  }
}
